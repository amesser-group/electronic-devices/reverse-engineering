=====================================
Reverse engineered electronic devices
=====================================

Overview
--------

Sometimes, when repairing electronic devices or just for interest,
its required to create a schematic for the device by examining its
printed circuit board. Sometimes these existing circuits can inspire 
new circuits, sometimes they help to learn electronic design.

This repository here contains the schematics of devices I had to retrieve,
either during repair or right before tearing them down.

Devices
-------

- Voltcraft FSP1243 24V Power Supply

Licensing
---------

CC BY-SA for all files

Copyright
---------

For all files in /hardware folder copyright is 
(c) 2020 Andreas Messer <andi@bastelmap.de>.





